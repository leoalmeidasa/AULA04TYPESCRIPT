class NaveEspacial{
    constructor(public propulsor: string){}

    entrandoNoHyperEspaco(){
        console.log(`Entrando no hyperespaço ${this.propulsor}`)
    }
    saindodoHyperEspaco(){
        console.log(`Saindo do hyperescapo ${this.propulsor}`)
    }
}

class MilleniumFalcon extends NaveEspacial implements Cargueiro{

    tipoCargo: number

    constructor(){
        super('HyperDrive')
        this.tipoCargo = 2
    }

    entrandoNoHyperEspaco(){
        if(Math.random()>=0.5){
            super.entrandoNoHyperEspaco()
        }else{console.log('Falhou para entrar no Hiperespaço')}
    }
}

let nave = new NaveEspacial('Hyperdrive')
nave.entrandoNoHyperEspaco()

let nave1= new NaveEspacial('LiquidFusion')
nave1.entrandoNoHyperEspaco()

let nave2= new NaveEspacial('Millenium Falcon')
nave2.entrandoNoHyperEspaco()

let nave3= new NaveEspacial('Battlefront')
nave3.saindodoHyperEspaco()

let Falcon = new MilleniumFalcon()
Falcon.entrandoNoHyperEspaco()

interface Cargueiro{
    tipoCargo: number
}

let boaParaTrabalho = ( nave: Cargueiro ) => nave.tipoCargo > 2
console.log(`A nave é boa para o trabalho? ${boaParaTrabalho ( Falcon ) ? 'sim': 'nao'}´)