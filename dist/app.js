"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var NaveEspacial = /** @class */ (function () {
    function NaveEspacial(propulsor) {
        this.propulsor = propulsor;
    }
    NaveEspacial.prototype.entrandoNoHyperEspaco = function () {
        console.log("Entrando no hyperespa\u00E7o " + this.propulsor);
    };
    NaveEspacial.prototype.saindodoHyperEspaco = function () {
        console.log("Saindo do hyperescapo " + this.propulsor);
    };
    return NaveEspacial;
}());
var MilleniumFalcon = /** @class */ (function (_super) {
    __extends(MilleniumFalcon, _super);
    function MilleniumFalcon() {
        var _this = _super.call(this, 'HyperDrive') || this;
        _this.tipoCargo = 2;
        return _this;
    }
    MilleniumFalcon.prototype.entrandoNoHyperEspaco = function () {
        if (Math.random() >= 0.5) {
            _super.prototype.entrandoNoHyperEspaco.call(this);
        }
        else {
            console.log('Falhou para entrar no Hiperespaço');
        }
    };
    return MilleniumFalcon;
}(NaveEspacial));
var nave = new NaveEspacial('Hyperdrive');
nave.entrandoNoHyperEspaco();
var nave1 = new NaveEspacial('LiquidFusion');
nave1.entrandoNoHyperEspaco();
var nave2 = new NaveEspacial('Millenium Falcon');
nave2.entrandoNoHyperEspaco();
var nave3 = new NaveEspacial('Battlefront');
nave3.saindodoHyperEspaco();
var Falcon = new MilleniumFalcon();
Falcon.entrandoNoHyperEspaco();
var boaParaTrabalho = function (nave) { return nave.tipoCargo > 2; };
console.log("A nave \u00E9 boa para o trabalho? " + (boaParaTrabalho(Falcon) ? 'sim' : 'nao') + "\u00B4)");
